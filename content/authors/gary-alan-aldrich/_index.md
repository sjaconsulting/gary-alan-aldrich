---
# Display name
name: Gary Alan Aldrich

# Username (this should match the folder name)
authors:
- gary-alan-aldrich

# Is this the primary user of the site?
superuser: true

# Role/position
role: Author

# Organizations/Affiliations
organizations:
- name: ""
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: Retired programmer interested in playing bridge, Sudoku, and writing.

interests:
- Sudoku
- Bridge
- Writing

education:
  courses:

  - course: MA in Mathematics
    institution: University of California Berkeley
    year: 1958
  - course: BA in Mathematics
    institution: University of California Berkeley
    year: 1955
  - course: HS Diploma
    institution: Tamalpais High School
    year: 1951      

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups: []
---

Gary Alan Aldrich was born in Montana on October 20th, 1933. He moved around with his family during his childhood, eventually settling into the San Francisco Bay Area in the 1950's. He attended UC Berkeley, and after receiving his Bachelor's and Master's degrees in Mathematics, became a computer programmer. He retired after 35 years as senior programmer for UCSF Hospital Information Systems in 1991.

In his retirement, Gary enjoyed solving Sudoku, playing bridge, and writing and editing fiction. He loved collecting books, films, and music, and built an extensive library to suit his eclectic tastes. A resident of the city for over 50 years, Gary passed away in San Francisco on March 7th, 2019.
