---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Homepage"
subtitle: ""
summary: ""
authors: [gary-alan-aldrich]
tags: []
categories: []
date: 2014-04-22T12:25:48-08:00
lastmod: 2015-08-22T12:25:48-08:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

At the present time, two major section exist, one on sudoku instruction, and a second on fiction. Future additional sections are planned, one on word games (anagrams, metaphors, palindromes, and specialized word lists), and one for miscellaneous (to which Bridge Probabilities shall be moved). For the time being, the text on Bridge Probabilities is placed at the end of fiction.

The sudoku instruction is arranged with the subpattern analysis textbook first, with all the subsidiary texts following it.

The section of fiction is meant to begin with the three novels, followed by four novelettes, followed by short stories and poetry. Temporarily, the textbook on bridge statistics is placed at the end of the fiction. It shall be moved to a miscellaneous section in the near future. In addition, a section is planned, as mentioned above, for word games.
