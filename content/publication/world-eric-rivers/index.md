---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "The World of Eric Rivers"
authors: [gary-alan-aldrich]
date: 2014-03-18T11:53:00-08:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2019-12-29T11:53:00-08:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: ""

# Summary. An optional shortened abstract.
summary: ""

tags: [Novels]
categories: [Fiction]
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
 - name: Paperback
   url: https://www.amazon.com/dp/1539879267
   icon_pack: fab
   icon: amazon
 - name: Kindle eBook
   url: https://www.amazon.com/dp/B086JS16DR
   icon_pack: fab
   icon: amazon   

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: "Cover for The World of Eric Rivers"
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

The coming of age of a young boy during the middle of the last century, beginning in Mill Valley in the late fifties and carried on a time trip through Santa Rosa and Montana before ending up in the Haight Ashbury during the sixties.
