+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Accomplish&shy;ments"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "San Francisco Unit of the American Contract Bridge League"
  organization_url = ""
  title = "Sectional Master Unit 506"
  url = ""
  certificate_url = ""
  date_start = "2007-08-01"
  date_end = ""
  description = ""

[[item]]
  organization = "San Francisco Unit of the American Contract Bridge League"
  organization_url = ""
  title = "Club Master Unit 506"
  url = ""
  certificate_url = ""
  date_start = "2005-10-01"
  date_end = ""
  description = ""

[[item]]
  organization = "The Medical Center at the University of California, San Francisco"
  organization_url = ""
  title = "Retirement"
  url = ""
  certificate_url = ""
  date_start = "1991-03-13"
  date_end = ""
  description = "Senior Programmer, UCSF Hospital Information Systems"
  
[[item]]
  organization = "The Medical Center at the University of California, San Francisco"
  organization_url = ""
  title = "Special Performance Award"
  url = ""
  certificate_url = ""
  date_start = "1988-07-01"
  date_end = ""
  description = ""

[[item]]
  organization = "Department of the Army"
  organization_url = ""
  title = "Official Commendation"
  url = ""
  certificate_url = ""
  date_start = "1966-01-18"
  date_end = ""
  description = "Officially Commended for his sustained superior performance during the period 1 November 1964 to 30 October 1965. Mr. Aldrich has accomplished his duties and responsibilities as a Systems Analyst with outstanding logic and ingenuity. His technical knowledge of automatic data processing, mathematics and systems enables him to provide exceptional systems and procedures for the Data Processing Services Division and functional areas. Mr. Aldrich's dedication to duty, keen interest, and his desire to excel in his profession, have enabled him to perform in a consistently exceptional manner."  

[[item]]
  organization = "Pi Mu Epsilon, California Beta"
  organization_url = ""
  title = "Certificate of Membership"
  url = ""
  certificate_url = ""
  date_start = "1955-05-21"
  date_end = ""
  description = "Honorary Mathematics Fraternity"

+++
